﻿namespace DotnetCoreWebApiDapper.SqlQueries
{
    public class SqlQuery
    {
        public string GetCategories => "SELECT * FROM Category";

        public string GetMovies =>
            @"SELECT movie.*, category.*, director.*
            FROM Movie movie 
            INNER JOIN Category category ON category.Id == movie.CategoryId
            INNER JOIN Director director ON director.Id == movie.DirectorId";
    }
}
