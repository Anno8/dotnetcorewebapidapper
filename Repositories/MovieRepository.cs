﻿using System;
using System.Collections.Generic;
using Dapper;
using DotnetCoreWebApiDapper.Entities;
using DotnetCoreWebApiDapper.SqlQueries;

namespace DotnetCoreWebApiDapper.Repositories
{
    public class MovieRepository : BaseRepository, IMovieRepository
    {
        private readonly SqlQuery _query;

        public MovieRepository(IDatabaseConnectionFactory connectionFactory, SqlQuery query) : base(connectionFactory)
        {
            _query = query;
        }

        public IEnumerable<Movie> Get()
        {
            using (var connection = _connectionFactory.Create())
            {
                return connection.Query<Movie, Category, Director, Movie>(_query.GetMovies, GetMoviesMap(new Dictionary<int, Movie>()));
            }
        }

        public Movie Get(int id)
        {
            throw new NotImplementedException();
        }

        private Func<Movie, Category, Director, Movie> GetMoviesMap(Dictionary<int, Movie> lookupDictionary)
        {
            Func<Movie, Category, Director, Movie> mapFunction = (movie, category, director) =>
            {
                if (!lookupDictionary.TryGetValue(movie.Id, out var lookedUpMovie))
                    lookupDictionary.Add(movie.Id, lookedUpMovie = movie);

                if (lookedUpMovie.Category == null)
                    lookedUpMovie.Category = category;

                if (lookedUpMovie.Director == null)
                    lookedUpMovie.Director = director;

                return lookedUpMovie;
            };

            return mapFunction;
        }
    }
}
