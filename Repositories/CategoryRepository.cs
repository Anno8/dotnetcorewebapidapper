﻿using System;
using System.Collections.Generic;
using Dapper;
using DotnetCoreWebApiDapper.Entities;
using DotnetCoreWebApiDapper.SqlQueries;

namespace DotnetCoreWebApiDapper.Repositories
{
    public class CategoryRepository : BaseRepository, ICategoryRepository
    {
        private readonly SqlQuery _query;

        public CategoryRepository(IDatabaseConnectionFactory connectionFactory, SqlQuery query) : base(connectionFactory)
        {
            _query = query;
        }

        public IEnumerable<Category> Get()
        {
            using (var connection = _connectionFactory.Create())
            {
                return connection.Query<Category>(_query.GetCategories);
            }
        }

        public Category Get(int id)
        {
            throw new NotImplementedException();
        }

    }
}
