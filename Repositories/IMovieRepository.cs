﻿using System.Collections.Generic;
using DotnetCoreWebApiDapper.Entities;

namespace DotnetCoreWebApiDapper.Repositories
{
    public interface IMovieRepository
    {
        IEnumerable<Movie> Get();

        Movie Get(int id);
    }
}
