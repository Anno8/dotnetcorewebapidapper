﻿using System.Data.Common;
using Microsoft.Extensions.Configuration;

namespace DotnetCoreWebApiDapper.Repositories
{
    public class DatabaseConnectionFactory : IDatabaseConnectionFactory
    {
        private readonly DbProviderFactory _providerFactory;
        private readonly string connectionString;

        public DatabaseConnectionFactory(DbProviderFactory providerFactory, IConfiguration configuration)
        {
            _providerFactory = providerFactory;
            connectionString = configuration["ConnectionString"];
        }

        public DbConnection Create()
        {
            var connection = _providerFactory.CreateConnection();
            connection.ConnectionString = connectionString;
            return connection;
        }
    }
}
