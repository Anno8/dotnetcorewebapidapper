﻿using System.Data.Common;

namespace DotnetCoreWebApiDapper.Repositories
{
    public interface IDatabaseConnectionFactory
    {
        DbConnection Create();
    }
}
