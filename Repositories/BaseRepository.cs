﻿using Dapper;

namespace DotnetCoreWebApiDapper.Repositories
{
    public abstract class BaseRepository
    {
        protected readonly IDatabaseConnectionFactory _connectionFactory;

        protected BaseRepository(IDatabaseConnectionFactory connectionFactory)
        {
            DefaultTypeMap.MatchNamesWithUnderscores = true;
            _connectionFactory = connectionFactory;
        }
    }
}
