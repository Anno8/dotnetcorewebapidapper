﻿using System.Collections.Generic;
using DotnetCoreWebApiDapper.Entities;

namespace DotnetCoreWebApiDapper.Repositories
{
    public interface ICategoryRepository
    {
        IEnumerable<Category> Get();

        Category Get(int id);
    }
}
