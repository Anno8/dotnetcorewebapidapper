﻿using System.Collections.Generic;
using DotnetCoreWebApiDapper.Entities;
using DotnetCoreWebApiDapper.Repositories;
using Microsoft.AspNetCore.Mvc;

namespace DotnetCoreWebApiDapper.Controllers
{
    [Route("api/[controller]")]
    public class MoviesController : ControllerBase
    {
        private readonly IMovieRepository _movieRepository;

        public MoviesController(IMovieRepository MovieRepository)
        {
            _movieRepository = MovieRepository;
        }

        [HttpGet]
        public IEnumerable<Movie> Get()
        {
            return _movieRepository.Get();
        }

        [HttpGet("{id}")]
        public Movie Get(int id)
        {
            return _movieRepository.Get(id);
        }

        [HttpPost]
        public Movie Post([FromBody] Movie Movie)
        {
            return null;
        }

        [HttpPut("{id}")]
        public Movie Put(int id, [FromBody] Movie Movie)
        {
            return null;
        }

        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}
