﻿using System.Collections.Generic;
using DotnetCoreWebApiDapper.Entities;
using DotnetCoreWebApiDapper.Repositories;
using Microsoft.AspNetCore.Mvc;

namespace DotnetCoreWebApiDapper.Controllers
{
    [Route("api/[controller]")]
    public class CategoriesController : ControllerBase
    {
        private readonly ICategoryRepository _categoryRepository;

        public CategoriesController(ICategoryRepository categoryRepository)
        {
            _categoryRepository = categoryRepository;
        }

        [HttpGet]
        public IEnumerable<Category> Get()
        {
            return _categoryRepository.Get();
        }

        [HttpGet("{id}")]
        public Category Get(int id)
        {
            return _categoryRepository.Get(id);
        }

        [HttpPost]
        public Category Post([FromBody] Category category)
        {
            return null;
        }

        [HttpPut("{id}")]
        public Category Put(int id, [FromBody] Category category)
        {
            return null;
        }

        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}
