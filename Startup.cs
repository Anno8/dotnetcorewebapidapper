﻿using System.Data.Common;
using DotnetCoreWebApiDapper.Repositories;
using DotnetCoreWebApiDapper.SqlQueries;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Data.Sqlite;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Swashbuckle.AspNetCore.Swagger;

namespace DotnetCoreWebApiDapper
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_1);

            services.AddSwaggerGen(c => c.SwaggerDoc("v1", new Info { Title = "Movies Web API", Version = "v1" }));

            // Dependency registration
            services.AddSingleton<IConfiguration>(Configuration);
            services.AddSingleton<DbProviderFactory>(_ => SqliteFactory.Instance);
            services.AddTransient<IDatabaseConnectionFactory, DatabaseConnectionFactory>();
            services.AddTransient(_ => new SqlQuery());
            services.AddTransient<IDirectorRepository, DirectorRepository>();
            services.AddTransient<ICategoryRepository, CategoryRepository>();
            services.AddTransient<IMovieRepository, MovieRepository>();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseHsts();
            }
            // configure CORS
            app.UseCors(builder => builder.AllowAnyOrigin().AllowAnyHeader().AllowAnyMethod());

            app.UseSwagger();
            app.UseSwaggerUI(c => c.SwaggerEndpoint("/swagger/v1/swagger.json", "Movies Web API v1"));

            app.UseHttpsRedirection();
            app.UseMvc();
        }
    }
}
