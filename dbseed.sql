DROP TABLE IF EXISTS Director;
DROP TABLE IF EXISTS Category;
DROP TABLE IF EXISTS Movie;

CREATE TABLE Director
( 
    Id INTEGER  NOT NULL PRIMARY KEY AUTOINCREMENT,
    Name TEXT NOT NULL,
    Age INTEGER 
);

CREATE TABLE Category
(
     Id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
     Name TEXT NOT NULL 
);

CREATE TABLE Movie
(
	  Id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
	  Name TEXT NOT NULL,
	  Description TEXT,
		CategoryId integer NOT NULL,
    DirectorId integer NOT NULL,
    FOREIGN KEY(CategoryId) REFERENCES Category(Id),
    FOREIGN KEY(DirectorId) REFERENCES Director(Id)
);

INSERT INTO Category ( Name ) VALUES ('Action');
INSERT INTO Category ( Name ) VALUES ('Drama');

INSERT INTO Director ( Name, Age) VALUES ('Dean DeBlois' , 48);
INSERT INTO Director ( Name, Age) VALUES ('James Cameron' , 64);

INSERT INTO Movie ( Name, Description, CategoryId, DirectorId ) VALUES ('How to train your dragon', 'Great How to train your dragon movie', 1, 1);
INSERT INTO Movie ( Name, Description, CategoryId, DirectorId ) VALUES ('Lilo & Stich', 'Great Lilo & Stich movie', 1, 1);

INSERT INTO Movie ( Name, Description, CategoryId, DirectorId ) VALUES ('Avatar', 'Great Avatar movie', 2, 2);

