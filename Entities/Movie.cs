﻿namespace DotnetCoreWebApiDapper.Entities
{
    public class Movie
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public Category Category { get; set; }
        public Director Director { get; set; }
    }
}
